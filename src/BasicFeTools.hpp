/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */
// #include <BasicFiniteElements.hpp>
unsigned int ittt = 0;
// namespace ContactPlasticity {
struct RollerData {
  const VectorDouble cCoords;
  const VectorDouble rollerDisp;
  VectorDouble rollerDispScaled;
  const double rAdius;
  double gAp;
  FTensor::Index<'i', 2> i;
  FTensor::Tensor1<double, 2> t_norm;

  RollerData(VectorDouble c_coords, VectorDouble roller_disp, double radius)
      : cCoords(c_coords), rollerDisp(roller_disp), rAdius(radius) {
    rollerDispScaled = rollerDisp;
  }

template <typename T>
inline FTensor::Tensor1<double, 2>&
getNormal(FTensor::Tensor1<T, 3> &t_coords) {
  VectorDouble pos = cCoords + rollerDispScaled;
  FTensor::Tensor1<double, 2> t_center(pos(0), pos(1));
  FTensor::Tensor1<double, 2> t_d;
  t_d(i) = t_coords(i) - t_center(i);
  const double dist = sqrt(t_d(i) * t_d(i));
  this->t_norm(i) = t_d(i) / dist;
  this->gAp = -std::fabs(dist - rAdius);
  this->gAp = -t_norm(i) * (t_d(i) - t_norm(i) * rAdius);
  if(this->gAp > 0 && ittt++ % 20 == 0)
    cout << "GAP > ZERO: " << this->gAp << endl;
  return t_norm;
};

template <typename T> inline double &getGap(FTensor::Tensor1<T, 3> &t_coords) {
  auto norm = getNormal(t_coords);
  return gAp;
};
};

MoFEMErrorCode get_options_from_command_line() {
  MoFEMFunctionBeginHot;

  char default_options[] =
      "-pc_type lu -pc_factor_mat_solver_package mumps "
      "-snes_atol 1e-8 -snes_rtol 1e-8 -snes_max_linear_solve_fail -1 "
      "-snes_linesearch_type basic -snes_max_it 25 "
      "-ts_max_snes_failures -1 "
      "-ts_error_if_step_fails false -ts_monitor ";

  CHKERR PetscOptionsInsertString(NULL, default_options);

  CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "", "none");
  // Set approximation order
  CHKERR PetscOptionsInt("-order", "approximation order", "", order, &order,
                         PETSC_NULL);
  CHKERR PetscOptionsScalar("-gravity_y", "Gravity load in Y direction", "",
                            spring_stiffness, &spring_stiffness, PETSC_NULL);

  CHKERR PetscOptionsBool("-no_output", "save no output files", "", no_output,
                          &no_output, PETSC_NULL);
  CHKERR PetscOptionsInt("-atom_test", "number of atom test", "", atom_test_nb,
                         &atom_test_nb, PETSC_NULL);

  // for elastic
  CHKERR PetscOptionsScalar("-young", "Young's modulus", "", young_modulus,
                            &young_modulus, PETSC_NULL);
  CHKERR PetscOptionsScalar("-poisson", "Poisson ratio", "", poisson_ratio,
                            &poisson_ratio, PETSC_NULL);

  // for plastic
  CHKERR PetscOptionsBool("-with_plasticity",
                          "run calculations with plasticity", "",
                          with_plasticity, &with_plasticity, PETSC_NULL);

  CHKERR PetscOptionsScalar("-sigmaY", "SigmaY - limit stress", "", sigmaY,
                            &sigmaY, PETSC_NULL);
  CHKERR PetscOptionsScalar("-H", "Hardening modulus", "", H, &H, PETSC_NULL);
  CHKERR PetscOptionsScalar("-cn", "cn parameter for active set constrains", "",
                            cn, &cn, PETSC_NULL);

  // for contact
  CHKERR PetscOptionsBool("-with_contact", "run calculations with contact", "",
                          with_contact, &with_contact, PETSC_NULL);
  CHKERR PetscOptionsScalar("-spring",
                            "Springs stiffness on the boundary (contact)", "",
                            spring_stiffness, &spring_stiffness, PETSC_NULL);

  if (with_contact) {
    for (int i = 0; i < MAX_NB_OF_ROLLERS; ++i) {
      int nb_coord = 2;
      int nb_disp = 2;
      double center_coords[2] = {0, -1};
      double disp[2] = {0, 0};
      PetscBool flg = PETSC_FALSE;
      PetscBool rflg;
      string param = "-roller" + to_string(i);
      CHKERR PetscOptionsGetRealArray(NULL, NULL, param.c_str(), center_coords,
                                      &nb_coord, &rflg);
      if (rflg) {
        param = "-move" + to_string(i);
        CHKERR PetscOptionsGetRealArray(NULL, NULL, param.c_str(), disp,
                                        &nb_disp, &flg);
        double radi;
        param = "-radius" + to_string(i);
        CHKERR PetscOptionsScalar(param.c_str(), "set roller radius", "", radi,
                                  &radi, &flg);
        if (nb_coord < 2 || !flg) {
          SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                  "provide an appropriate number of coords for roller center "
                  "and corresponding radius");
        }
        VectorDouble center;
        VectorDouble displ;
        center.data().assign(center_coords, center_coords + 2);
        displ.data().assign(disp, disp + 2);
        rollerData.push_back(RollerData(center, displ, radi));
      }
    }
  }

  ierr = PetscOptionsEnd();
  CHKERRQ(ierr);

  MoFEMFunctionReturnHot(0);
}

/**
 * \brief Not used at this stage. Could be used to do some calculations,
 * before assembly of local elements.
 */
struct FePrePostProcess : public FEMethod {

  vector<RollerData> &rollerData;
  boost::ptr_vector<MethodForForceScaling> methodsOp;

  FePrePostProcess(vector<RollerData> &roller_data) : rollerData(roller_data) {}

  MoFEMErrorCode preProcess() {
    //
    MoFEMFunctionBegin;
    switch (ts_ctx) {
    case CTX_TSSETIJACOBIAN: {
      snes_ctx = CTX_SNESSETJACOBIAN;
      snes_B = ts_B;
      break;
    }
    case CTX_TSSETIFUNCTION: {
      snes_ctx = CTX_SNESSETFUNCTION;
      snes_f = ts_F;
      break;
    }
    default:
      break;
    }
      if (!rollerData.empty()) {
        for (auto &roller : rollerData) {
          roller.rollerDispScaled = roller.rollerDisp;
          CHKERR MethodForForceScaling::applyScale(this, methodsOp,
                                                   roller.rollerDispScaled);
        }
      }
    MoFEMFunctionReturn(0);
  }

  MoFEMErrorCode postProcess() { return 0; }
};

MoFEMErrorCode calculateCentroid(MoFEM::Interface &mField, const Range ents,
                                 VectorDouble &output, double &rad) {
  MoFEMFunctionBegin;
  FTensor::Index<'i', 3> i;

  auto get_skin = [&](Range ents) {
    Range faces;
    EntityHandle meshset;
    CHKERR mField.get_moab().create_meshset(MESHSET_SET, meshset);
    CHKERR mField.get_moab().add_entities(meshset, ents);

    CHKERR mField.get_moab().get_entities_by_type(meshset, MBTRI, faces);
    Skinner skin(&mField.get_moab());
    Range skin_edges;
    CHKERR skin.find_skin(0, faces, false, skin_edges);

    CHKERR mField.get_moab().delete_entities(&meshset, 1);
    return skin_edges;
  };

  auto get_coords_from_ent = [&](EntityHandle ent) {
    const EntityHandle *conn;
    int num_nodes;
    CHKERR mField.get_moab().get_connectivity(ent, conn, num_nodes, true);
    MatrixDouble coords;
    coords.resize(num_nodes, 3, false);
    CHKERR mField.get_moab().get_coords(conn, num_nodes,
                                        &*coords.data().begin());
    return coords;
  };

  auto get_centroid = [&](EntityHandle ent) {
    auto coords = get_coords_from_ent(ent);
    FTensor::Tensor1<FTensor::PackPtr<double *, 3>, 3> t_coords(
        &coords(0, 0), &coords(0, 1), &coords(0, 2));

    FTensor::Tensor1<double, 3> t_center;
    t_center(i) = 0;
    for (int rr = 0; rr != coords.size1(); ++rr) {
      t_center(i) += t_coords(i);
      ++t_coords;
    }
    t_center(i) /= coords.size1();
    return t_center;
  };

  output.resize(3);
  output.clear();
  int cc = 0;
  Range skin_ents = get_skin(ents);

  for (auto ent : skin_ents) {
    auto t_center = get_centroid(ent);
    for (int i : {0, 1, 2})
      output(i) += t_center(i);
    ++cc;
  }

  for (int i : {0, 1, 2})
    output(i) /= cc;

  auto get_radius = [&](EntityHandle ent) {
    double radi = 0.;
    auto coords = get_coords_from_ent(ent);
    FTensor::Tensor1<double *, 3> t_coords(&coords(0, 0), &coords(0, 1),
                                           &coords(0, 2));
    FTensor::Tensor1<double, 3> t_cen(output(0), output(1), output(2));
    FTensor::Tensor1<double, 3> t_d;
    t_d(i) = t_coords(i) - t_cen(i);
    radi = sqrt(t_d(i) * t_d(i));
    return radi;
  };

  rad = get_radius(*skin_ents.begin());

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode get_roller_parameters(MoFEM::Interface &mField) {
  MoFEMFunctionBegin;

  auto remove_entities_from_meshsets = [&](Range ents_to_remove) {
    MoFEMFunctionBegin;
    // Remove entities from meshsets
    Range meshsets;
    CHKERR mField.get_moab().get_entities_by_type(0, MBENTITYSET, meshsets,
                                                  false);
    for (auto m : meshsets)
      CHKERR mField.get_moab().remove_entities(m, ents_to_remove);
    mField.get_moab().delete_entities(ents_to_remove);
    MoFEMFunctionReturn(0);
  };

  Range all_tris;
  CHKERR mField.get_moab().get_entities_by_type(0, MBTRI, all_tris, false);
  cerr << all_tris << endl;

  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {

    if (it->getName().compare(0, 6, "ROLLER") == 0) {
      Range tris;
      std::vector<double> r_params;
      CHKERR it->getMeshsetIdEntitiesByDimension(mField.get_moab(), 2, tris,
                                                 true);
      it->getAttributes(r_params);
      VectorDouble displacement(2);
      if (r_params.size() > 1) {
        displacement(0) = r_params[1];
        displacement(1) = r_params[2];
      }
      VectorDouble centroid;
      double radiuss;
      CHKERR calculateCentroid(mField, tris, centroid, radiuss);
      centroid.resize(2); //FIXME: for 2D
      rollerData.push_back(RollerData(centroid, displacement, radiuss));

      CHKERR remove_entities_from_meshsets(tris);
    }
  }
  Range tris_after_delete;
  CHKERR mField.get_moab().get_entities_by_type(0, MBTRI, all_tris, false);
  cerr << all_tris << endl;
 
  MoFEMFunctionReturn(0);
}

// }; // namespace OpContactTools
