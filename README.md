# Installing MoFEM

Follow installation of MoFEM with the source code described
[here](http://mofem.eng.gla.ac.uk/mofem/html/install_spack.html).

## Cloning module source code

MoFEM core source code is basic installation. Source code of particular
projected is located in stand alone repository. This module has source code
located in bitbucket repository.

```
cd $HOME/mofem-cephas/mofem/users_modules
git clone git@bitbucket.org:karol41/um_multifield_plasticity.git multifield_plasticity
# go to users modules build directory
touch CMakeCache.txt
make -j4
```


## Plasticity 

To run 2D cantilever beam example execute the following command line:

~~~~~
./multifield_plasticity -file_name ./examples/2D_cantilever.cub \
-ts_dt 0.04 -ts_final_time 0.6 -ts_adapt_type none -with_plasticity \
-load_history load_history.in \
-young 210e3 -poisson 0.3 -sigmaY 240.0 -H 1e-2 \
-order 2 | tee log
~~~~~


## Create output VTK script

~~~~~
./do_vtk.sh
~~~~~

## Produce load-displacement plot in gnuplot

~~~~~
grep "Uy time" log | awk '{print $3 " " $5}' | tee log_upd
~~~~~

~~~~~
gnuplot -e "set terminal x11; plot 'log_upd' using (-\$2):(\$1) with linespoints; pause -1"
~~~~~
