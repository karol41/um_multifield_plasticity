/**
 * \file lesson8_contact.cpp
 * \example lesson8_contact.cpp
 *
 * ContactPlasticity of contact problem
 *
 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <MoFEM.hpp>

using namespace MoFEM;

using EntData = DataForcesAndSourcesCore::EntData;
using DomainEle = PipelineManager::FaceEle2D;
using DomainEleOp = DomainEle::UserDataOperator;
using BoundaryEle = PipelineManager::EdgeEle2D;
using BoundaryEleOp = BoundaryEle::UserDataOperator;


int order = 2;
PetscBool no_output = PETSC_FALSE;
int atom_test_nb = 0;

constexpr int MAX_NB_OF_ROLLERS = 5;

// for elastic
double young_modulus = 10;
double poisson_ratio = 0.25;

//for plastic
PetscBool with_plasticity = PETSC_FALSE;
double sigmaY = 5.0;
double H = 1.0;
double cn = 1.;

// for contact
PetscBool with_contact = PETSC_FALSE;
struct RollerData;
vector<RollerData> rollerData;
double spring_stiffness = 1e-6;
double gravity_y = 0.;


#include <BasicFiniteElements.hpp>

#include <BasicFeTools.hpp>

#include <ElasticOperators.hpp>
#include <PlasticOperators.hpp>
#include <ContactOperators.hpp>

using namespace OpElasticTools;
using namespace OpPlasticTools;
using namespace OpContactTools;

struct ContactPlasticity {

  ContactPlasticity(MoFEM::Interface &m_field) : mField(m_field) {}

  MoFEMErrorCode runProblem();

private:
  MoFEM::Interface &mField;

  MoFEMErrorCode setUP();
  MoFEMErrorCode createCommonData();
  MoFEMErrorCode bC();
  MoFEMErrorCode OPs();
  MoFEMErrorCode tsSolve();
  MoFEMErrorCode postProcess();
  MoFEMErrorCode checkResults();

  MatrixDouble invJac, jAc;
  boost::shared_ptr<OpContactTools::CommonData> commonDataPtr;
  boost::shared_ptr<PostProcFaceOnRefinedMeshFor2D> postProcFe;
  std::tuple<SmartPetscObj<Vec>, SmartPetscObj<VecScatter>> uXScatter;
  std::tuple<SmartPetscObj<Vec>, SmartPetscObj<VecScatter>> uYScatter;

  //mofem boundary conditions
  boost::ptr_map<std::string, NeumannForcesSurface> neumann_forces;
  boost::ptr_map<std::string, NodalForce> nodal_forces;
  boost::ptr_map<std::string, EdgeForce> edge_forces;
  boost::shared_ptr<DirichletDisplacementBc> dirichlet_bc_ptr;

  Range getEntsOnMeshSkin();
};

MoFEMErrorCode ContactPlasticity::runProblem() {
  MoFEMFunctionBegin;
  CHKERR setUP();
  CHKERR createCommonData();
  CHKERR bC();
  CHKERR OPs();
  CHKERR tsSolve();
  CHKERR postProcess();
  CHKERR checkResults();
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ContactPlasticity::setUP() {
  MoFEMFunctionBegin;
  Simple *simple = mField.getInterface<Simple>();
  // Add field
  CHKERR simple->addDomainField("U", H1, AINSWORTH_LEGENDRE_BASE, 2);
  CHKERR simple->addBoundaryField("U", H1, AINSWORTH_LEGENDRE_BASE, 2);
  CHKERR simple->setFieldOrder("U", order);
  if (with_contact) {

    CHKERR simple->addDomainField("SIGMA", HCURL, DEMKOWICZ_JACOBI_BASE, 2);
    CHKERR simple->addBoundaryField("SIGMA", HCURL, DEMKOWICZ_JACOBI_BASE, 2);
    CHKERR simple->setFieldOrder("SIGMA", 0);
    auto skin_edges = getEntsOnMeshSkin();
    CHKERR simple->setFieldOrder("SIGMA", order - 1, &skin_edges);
  }
  // CHKERR simple->setFieldOrder("U", order + 1, &skin_edges);

  if (with_plasticity) {
    CHKERR simple->addDomainField("TAU", L2, AINSWORTH_LEGENDRE_BASE, 1);
    CHKERR simple->addDomainField("EP", L2, AINSWORTH_LEGENDRE_BASE, 3);
    CHKERR simple->setFieldOrder("TAU", order - 1);
    CHKERR simple->setFieldOrder("EP", order - 1);
  }
  PetscBool is_partitioned = PETSC_FALSE; //FIXME: work on partitioned meshes

  CHKERR simple->defineFiniteElements();

  // Add Neumann forces elements
  CHKERR MetaNeumannForces::addNeumannBCElements(mField, "U");
  CHKERR MetaNodalForces::addElement(mField, "U");
  CHKERR MetaEdgeForces::addElement(mField, "U");

  simple->getOtherFiniteElements().push_back("FORCE_FE");
  simple->getOtherFiniteElements().push_back("PRESSURE_FE");

  CHKERR simple->defineProblem(is_partitioned);
  CHKERR simple->buildFields();
  CHKERR simple->buildFiniteElements();
  CHKERR simple->buildProblem();

  // CHKERR simple->setUp(); //TODO: 
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ContactPlasticity::createCommonData() {
  MoFEMFunctionBegin;

  auto get_material_stiffness = [&](FTensor::Ddg<double, 2, 2> &t_D) {
    MoFEMFunctionBegin;
    FTensor::Index<'i', 2> i;
    FTensor::Index<'j', 2> j;
    FTensor::Index<'k', 2> k;
    FTensor::Index<'l', 2> l;
    t_D(i, j, k, l) = 0;

    double c = young_modulus / (1 - poisson_ratio * poisson_ratio);
    double o = poisson_ratio * c;

    t_D(0, 0, 0, 0) = c;
    t_D(0, 0, 1, 1) = o;

    t_D(1, 1, 0, 0) = o;
    t_D(1, 1, 1, 1) = c;

    t_D(0, 1, 0, 1) = (1 - poisson_ratio) * c;
    MoFEMFunctionReturn(0);
  };

  commonDataPtr = boost::make_shared<OpContactTools::CommonData>();
  CHKERR get_material_stiffness(commonDataPtr->tD);

  commonDataPtr->mGradPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mStrainPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->mStressPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->contactStressPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->contactStressDivergencePtr =
      boost::make_shared<MatrixDouble>();
  commonDataPtr->contactTractionPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->contactDispPtr = boost::make_shared<MatrixDouble>();

  commonDataPtr->plasticSurfacePtr = boost::make_shared<VectorDouble>();
  commonDataPtr->plasticFlowPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->plasticTauPtr = boost::make_shared<VectorDouble>();
  commonDataPtr->plasticTauDotPtr = boost::make_shared<VectorDouble>();
  commonDataPtr->plasticStrainPtr = boost::make_shared<MatrixDouble>();
  commonDataPtr->plasticStrainDotPtr = boost::make_shared<MatrixDouble>();

  jAc.resize(2, 2, false);
  invJac.resize(2, 2, false);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ContactPlasticity::bC() {
  MoFEMFunctionBegin;

  auto fix_disp = [&](const std::string blockset_name) {
    Range fix_ents;
    for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
      if (it->getName().compare(0, blockset_name.length(), blockset_name) ==
          0) {
        CHKERR mField.get_moab().get_entities_by_handle(it->meshset, fix_ents,
                                                        true);
      }
    }
    return fix_ents;
  };

  auto remove_ents = [&](const Range &&ents, const bool fix_x,
                         const bool fix_y) {
    auto prb_mng = mField.getInterface<ProblemsManager>();
    auto simple = mField.getInterface<Simple>();
    MoFEMFunctionBegin;
    Range verts;
    CHKERR mField.get_moab().get_connectivity(ents, verts, true);
    verts.merge(ents);
    const int lo_coeff = fix_x ? 0 : 1;
    const int hi_coeff = fix_y ? 1 : 0;
    CHKERR prb_mng->removeDofsOnEntities(simple->getProblemName(), "U", verts,
                                         lo_coeff, hi_coeff);
    MoFEMFunctionReturn(0);
  };

  // Range boundary_ents;
  // boundary_ents.merge(fix_disp("FIX_X"));
  // boundary_ents.merge(fix_disp("FIX_Y"));
  // boundary_ents.merge(fix_disp("FIX_ALL"));
  // CHKERR mField.getInterface<ProblemsManager>()->removeDofsOnEntities(
  //     mField.getInterface<Simple>()->getProblemName(), "U", boundary_ents, 0,
  //     2);

  CHKERR remove_ents(fix_disp("FIX_X"), true, false);
  CHKERR remove_ents(fix_disp("FIX_Y"), false, true);
  CHKERR remove_ents(fix_disp("FIX_ALL"), true, true);

  // forces and pressures on surface
  CHKERR MetaNeumannForces::setMomentumFluxOperators(mField, neumann_forces,
                                                     PETSC_NULL, "U");
  // nodal forces
  CHKERR MetaNodalForces::setOperators(mField, nodal_forces, PETSC_NULL, "U");
  // edge forces
  CHKERR MetaEdgeForces::setOperators(mField, edge_forces, PETSC_NULL, "U");

  for (auto mit = neumann_forces.begin(); mit != neumann_forces.end(); mit++) {
    mit->second->methodsOp.push_back(new TimeForceScale("-load_history", true));
  }
  for (auto mit = nodal_forces.begin(); mit != nodal_forces.end(); mit++) {
    mit->second->methodsOp.push_back(new TimeForceScale("-load_history", true));
  }
  for (auto mit = edge_forces.begin(); mit != edge_forces.end(); mit++) {
    mit->second->methodsOp.push_back(new TimeForceScale("-load_history", true));
  }

  dirichlet_bc_ptr = boost::make_shared<DirichletDisplacementBc>(mField, "U");
  dirichlet_bc_ptr->methodsOp.push_back(
      new TimeForceScale("-load_history", true));

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ContactPlasticity::OPs() {
  MoFEMFunctionBegin;
  PipelineManager *pipeline_mng = mField.getInterface<PipelineManager>();

  auto add_domain_base_ops = [&](auto &pipeline) {
    pipeline.push_back(new OpCalculateInvJacForFace(invJac));
    pipeline.push_back(new OpSetInvJacH1ForFace(invJac));
    if (with_contact) {
      pipeline.push_back(new OpMakeHdivFromHcurl());
      pipeline.push_back(new OpCalculateJacForFace(jAc));
      pipeline.push_back(new OpSetContravariantPiolaTransformFace(jAc));
      pipeline.push_back(new OpSetInvJacHcurlFace(invJac));
    }

    pipeline.push_back(
        new OpCalculateVectorFieldGradient<2, 2>("U", commonDataPtr->mGradPtr));
    pipeline.push_back(new OpStrain("U", commonDataPtr));

    if (with_plasticity) {

      pipeline.push_back(new OpCalculateScalarFieldValues(
          "TAU", commonDataPtr->plasticTauPtr, MBTRI));
      pipeline.push_back(new OpCalculateScalarFieldValuesDot(
          "TAU", commonDataPtr->plasticTauDotPtr, MBTRI));

      pipeline.push_back(new OpCalculateTensor2SymmetricFieldValues<2>(
          "EP", commonDataPtr->plasticStrainPtr, MBTRI));
      pipeline.push_back(new OpCalculateTensor2SymmetricFieldValuesDot<2>(
          "EP", commonDataPtr->plasticStrainDotPtr, MBTRI));

      pipeline.push_back(new OpPlasticStress("U", commonDataPtr));
      pipeline.push_back(new OpCalculatePlasticSurface("U", commonDataPtr));
    }
  };

  auto add_domain_ops_lhs = [&](auto &pipeline) {
    pipeline.push_back(new OpStiffnessMatrixLhs("U", "U", commonDataPtr));
    if (with_contact)
      pipeline.push_back(
          new OpConstrainDomainLhs_dU("SIGMA", "U", commonDataPtr));

    if (with_plasticity) {
      pipeline.push_back(
          new OpCalculatePlasticInternalForceLhs_dEP("U", "EP", commonDataPtr));
      pipeline.push_back(
          new OpCalculatePlasticFlowLhs_dU("EP", "U", commonDataPtr));
      pipeline.push_back(
          new OpCalculatePlasticFlowLhs_dEP("EP", "EP", commonDataPtr));
      pipeline.push_back(
          new OpCalculatePlasticFlowLhs_dTAU("EP", "TAU", commonDataPtr));

      pipeline.push_back(
          new OpCalculateContrainsLhs_dU("TAU", "U", commonDataPtr));
      pipeline.push_back(
          new OpCalculateContrainsLhs_dEP("TAU", "EP", commonDataPtr));
      pipeline.push_back(
          new OpCalculateContrainsLhs_dTAU("TAU", "TAU", commonDataPtr));
    }
  };

  auto add_domain_ops_rhs = [&](auto &pipeline) {
    auto gravity = [&](double x, double y) {
      return FTensor::Tensor1<double, 2>{0., gravity_y};
    };
    pipeline.push_back(new OpForceRhs("U", commonDataPtr, gravity));
    pipeline.push_back(
        new OpCalculateVectorFieldGradient<2, 2>("U", commonDataPtr->mGradPtr));
    if (with_plasticity) {
      pipeline.push_back(new OpCalculatePlasticFlowRhs("EP", commonDataPtr));
      pipeline.push_back(new OpCalculateContrainsRhs("TAU", commonDataPtr));
    } else {
      pipeline.push_back(new OpStrain("U", commonDataPtr));
      pipeline.push_back(new OpStress("U", commonDataPtr));
    }

    pipeline.push_back(new OpInternalForceRhs("U", commonDataPtr));

    if (with_contact) {
      pipeline.push_back(new OpCalculateVectorFieldValues<2>(
          "U", commonDataPtr->contactDispPtr));

      pipeline.push_back(new OpCalculateHVecTensorField<2, 2>(
          "SIGMA", commonDataPtr->contactStressPtr));
      pipeline.push_back(new OpCalculateHVecTensorDivergence<2, 2>(
          "SIGMA", commonDataPtr->contactStressDivergencePtr));
      pipeline.push_back(new OpConstrainDomainRhs("SIGMA", commonDataPtr));
      pipeline.push_back(new OpInternalDomainContactRhs("U", commonDataPtr));
    }
  };

  auto add_boundary_base_ops = [&](auto &pipeline) {
    pipeline.push_back(new OpSetContrariantPiolaTransformOnEdge());
    pipeline.push_back(new OpCalculateVectorFieldValues<2>(
        "U", commonDataPtr->contactDispPtr));
    pipeline.push_back(new OpConstrainBoundaryTraction("SIGMA", commonDataPtr));
  };

  auto add_boundary_ops_lhs = [&](auto &pipeline) {
    pipeline.push_back(
        new OpConstrainBoundaryLhs_dU("SIGMA", "U", commonDataPtr));
    pipeline.push_back(
        new OpConstrainBoundaryLhs_dTraction("SIGMA", "SIGMA", commonDataPtr));
    pipeline.push_back(new OpSpringLhs("U", "U"));
  };

  auto add_boundary_ops_rhs = [&](auto &pipeline) {
    for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
      if (it->getName().compare(0, 5, "FORCE") == 0) {
        Range my_edges;
        std::vector<double> force_vec;
        CHKERR it->getMeshsetIdEntitiesByDimension(mField.get_moab(), 1,
                                                   my_edges, true);
        it->getAttributes(force_vec);
        pipeline.push_back(new OpEdgeForceRhs("U", my_edges, force_vec));
      }
    }
    if (with_contact) {
      pipeline.push_back(new OpConstrainBoundaryRhs("SIGMA", commonDataPtr));
      pipeline.push_back(new OpSpringRhs("U", commonDataPtr));
    }
  };

  add_domain_base_ops(pipeline_mng->getOpDomainLhsPipeline());
  add_domain_base_ops(pipeline_mng->getOpDomainRhsPipeline());
  add_domain_ops_lhs(pipeline_mng->getOpDomainLhsPipeline());
  add_domain_ops_rhs(pipeline_mng->getOpDomainRhsPipeline());
  if (with_contact) {
    add_boundary_base_ops(pipeline_mng->getOpBoundaryLhsPipeline());
    add_boundary_base_ops(pipeline_mng->getOpBoundaryRhsPipeline());
    add_boundary_ops_lhs(pipeline_mng->getOpBoundaryLhsPipeline());
  }
  add_boundary_ops_rhs(pipeline_mng->getOpBoundaryRhsPipeline());

  auto integration_rule = [](int, int, int approx_order) { return 2 * order; };
  CHKERR pipeline_mng->setDomainRhsIntegrationRule(integration_rule);
  CHKERR pipeline_mng->setDomainLhsIntegrationRule(integration_rule);
  CHKERR pipeline_mng->setBoundaryRhsIntegrationRule(integration_rule);
  CHKERR pipeline_mng->setBoundaryLhsIntegrationRule(integration_rule);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ContactPlasticity::tsSolve() {
  MoFEMFunctionBegin;

  Simple *simple = mField.getInterface<Simple>();
  PipelineManager *pipeline_mng = mField.getInterface<PipelineManager>();
  ISManager *is_manager = mField.getInterface<ISManager>();

  // auto solver = pipeline_mng->createTS(); //TODO:
  auto create_custom_ts = [&]() {

    auto set_dm_section = [&](auto dm) {
      MoFEMFunctionBegin;
      PetscSection section;
      CHKERR mField.getInterface<ISManager>()->sectionCreate(
          simple->getProblemName(), &section);
      CHKERR DMSetDefaultSection(dm, section);
      CHKERR DMSetDefaultGlobalSection(dm, section);
      CHKERR PetscSectionDestroy(&section);
      MoFEMFunctionReturn(0);
    };
    auto dm = simple->getDM();
    CHKERR set_dm_section(dm);

    boost::shared_ptr<FEMethod> null;
    auto preProc = boost::make_shared<FePrePostProcess>(rollerData);
    preProc->methodsOp.push_back(new TimeForceScale("-load_history", true));

    // Add element to calculate lhs of stiff part
    if (pipeline_mng->getDomainLhsFE()) {

      CHKERR DMMoFEMTSSetIJacobian(dm, simple->getDomainFEName(), null,
                                   preProc, null);
      CHKERR DMMoFEMTSSetIJacobian(dm, simple->getDomainFEName(), null,
                                   dirichlet_bc_ptr, null);

      CHKERR DMMoFEMTSSetIJacobian(dm, simple->getDomainFEName(),
                                   pipeline_mng->getDomainLhsFE(), null, null);
      CHKERR DMMoFEMTSSetIJacobian(dm, simple->getDomainFEName(), null, null,
                                   dirichlet_bc_ptr);
      CHKERR DMMoFEMTSSetIJacobian(dm, simple->getDomainFEName(), null, null,
                                   preProc);
    }
    if (pipeline_mng->getBoundaryLhsFE())
      CHKERR DMMoFEMTSSetIJacobian(dm, simple->getBoundaryFEName(),
                                   pipeline_mng->getBoundaryLhsFE(), null, null);
    if (pipeline_mng->getSkeletonLhsFE())
      CHKERR DMMoFEMTSSetIJacobian(dm, simple->getSkeletonFEName(),
                                   pipeline_mng->getSkeletonLhsFE(), null, null);

    // Add element to calculate rhs of stiff part
    if (pipeline_mng->getDomainRhsFE()) {

      // add dirichlet boundary conditions
      CHKERR DMMoFEMTSSetIFunction(dm, simple->getDomainFEName(), null,
                                   preProc, null);
      CHKERR DMMoFEMTSSetIFunction(dm, simple->getDomainFEName(), null,
                                   dirichlet_bc_ptr, null);

      CHKERR DMMoFEMTSSetIFunction(dm, simple->getDomainFEName(),
                                   pipeline_mng->getDomainRhsFE(), null, null);

      // Add surface forces

      for (auto fit = neumann_forces.begin(); fit != neumann_forces.end();
           fit++) {
        CHKERR DMMoFEMTSSetIFunction(dm, fit->first.c_str(),
                                     &fit->second->getLoopFe(), NULL, NULL);
      }

      // Add edge forces
      for (auto fit = edge_forces.begin(); fit != edge_forces.end(); fit++) {
        cerr << fit->first.c_str() << endl;
        CHKERR DMMoFEMTSSetIFunction(dm, fit->first.c_str(),
                                     &fit->second->getLoopFe(), NULL, NULL);
      }

      // Add nodal forces
      for (auto fit = nodal_forces.begin(); fit != nodal_forces.end(); fit++) {
        CHKERR DMMoFEMTSSetIFunction(dm, fit->first.c_str(),
                                     &fit->second->getLoopFe(), NULL, NULL);
      }

      CHKERR DMMoFEMTSSetIFunction(dm, simple->getDomainFEName(), null, null,
                                   dirichlet_bc_ptr);
      CHKERR DMMoFEMTSSetIFunction(dm, simple->getDomainFEName(), null, null,
                                   preProc);
    }
    if (pipeline_mng->getBoundaryRhsFE())
      CHKERR DMMoFEMTSSetIFunction(dm, simple->getBoundaryFEName(),
                                   pipeline_mng->getBoundaryRhsFE(), null, null);
    if (pipeline_mng->getSkeletonRhsFE())
      CHKERR DMMoFEMTSSetIFunction(dm, simple->getSkeletonFEName(),
                                   pipeline_mng->getSkeletonRhsFE(), null, null);

    auto ts = MoFEM::createTS(mField.get_comm());
    CHKERR TSSetDM(ts, dm);
    return ts;
  };

  auto solver = create_custom_ts();

  CHKERR TSSetExactFinalTime(solver, TS_EXACTFINALTIME_STEPOVER);

  auto dm = simple->getDM();
  auto D = smartCreateDMVector(dm);

  // IMPORTANT!! for heterogeneous Dirichlet Boundary conditions
  CHKERR DMoFEMMeshToLocalVector(dm, D, INSERT_VALUES, SCATTER_FORWARD);

  CHKERR TSSetSolution(solver, D);
  CHKERR TSSetFromOptions(solver);
  CHKERR TSSetUp(solver);

  auto set_section_monitor = [&]() {
    MoFEMFunctionBegin;
    SNES snes;
    CHKERR TSGetSNES(solver, &snes);
    PetscViewerAndFormat *vf;
    CHKERR PetscViewerAndFormatCreate(PETSC_VIEWER_STDOUT_WORLD,
                                      PETSC_VIEWER_DEFAULT, &vf);
    CHKERR SNESMonitorSet(
        snes,
        (MoFEMErrorCode(*)(SNES, PetscInt, PetscReal, void *))SNESMonitorFields,
        vf, (MoFEMErrorCode(*)(void **))PetscViewerAndFormatDestroy);
    MoFEMFunctionReturn(0);
  };

  auto create_post_process_element = [&]() {
    MoFEMFunctionBegin;
    postProcFe = boost::make_shared<PostProcFaceOnRefinedMeshFor2D>(mField);
    postProcFe->generateReferenceElementMesh();
    postProcFe->getOpPtrVector().push_back(
        new OpCalculateInvJacForFace(invJac));
    postProcFe->getOpPtrVector().push_back(new OpSetInvJacH1ForFace(invJac));
    if (with_contact) {
      postProcFe->getOpPtrVector().push_back(new OpCalculateJacForFace(jAc));
      postProcFe->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());
      postProcFe->getOpPtrVector().push_back(
          new OpSetContravariantPiolaTransformFace(jAc));
      postProcFe->getOpPtrVector().push_back(new OpSetInvJacHcurlFace(invJac));
    }

    postProcFe->getOpPtrVector().push_back(
        new OpCalculateVectorFieldGradient<2, 2>("U", commonDataPtr->mGradPtr));
    postProcFe->getOpPtrVector().push_back(new OpStrain("U", commonDataPtr));
    if (with_plasticity) {
      postProcFe->getOpPtrVector().push_back(new OpCalculateScalarFieldValues(
          "TAU", commonDataPtr->plasticTauPtr, MBTRI));
      postProcFe->getOpPtrVector().push_back(
          new OpCalculateTensor2SymmetricFieldValues<2>(
              "EP", commonDataPtr->plasticStrainPtr, MBTRI));
      postProcFe->getOpPtrVector().push_back(
          new OpPlasticStress("U", commonDataPtr));
      postProcFe->getOpPtrVector().push_back(
          new OpCalculatePlasticSurface("U", commonDataPtr));
    } else {
      postProcFe->getOpPtrVector().push_back(new OpStress("U", commonDataPtr));
    }
    if (with_contact) {

      postProcFe->getOpPtrVector().push_back(
          new OpCalculateHVecTensorDivergence<2, 2>(
              "SIGMA", commonDataPtr->contactStressDivergencePtr));
      postProcFe->getOpPtrVector().push_back(
          new OpCalculateHVecTensorField<2, 2>(
              "SIGMA", commonDataPtr->contactStressPtr));

      postProcFe->getOpPtrVector().push_back(
          new OpPostProcElastic("U", postProcFe->postProcMesh,
                                postProcFe->mapGaussPts, commonDataPtr));
      postProcFe->getOpPtrVector().push_back(
          new OpPostProcContact("SIGMA", postProcFe->postProcMesh,
                                postProcFe->mapGaussPts, commonDataPtr));
    }
    postProcFe->getOpPtrVector().push_back(new OpPostProcElastic(
        "U", postProcFe->postProcMesh, postProcFe->mapGaussPts, commonDataPtr));

    if (with_plasticity) {
      postProcFe->getOpPtrVector().push_back(
          new OpPostProcPlastic("U", postProcFe->postProcMesh,
                                postProcFe->mapGaussPts, commonDataPtr));
    }
    postProcFe->addFieldValuesPostProc("U");
    MoFEMFunctionReturn(0);
  };

  auto scatter_create = [&](auto coeff) {
    SmartPetscObj<IS> is;
    CHKERR is_manager->isCreateProblemFieldAndRank(simple->getProblemName(),
                                                   ROW, "U", coeff, coeff, is);
    int loc_size;
    CHKERR ISGetLocalSize(is, &loc_size);
    Vec v;
    CHKERR VecCreateMPI(mField.get_comm(), loc_size, PETSC_DETERMINE, &v);
    VecScatter scatter;
    CHKERR VecScatterCreate(D, is, v, PETSC_NULL, &scatter);
    return std::make_tuple(SmartPetscObj<Vec>(v),
                           SmartPetscObj<VecScatter>(scatter));
  };

  auto set_time_monitor = [&]() {
    MoFEMFunctionBegin;
    boost::shared_ptr<OpContactTools::Monitor> monitor_ptr(
        new OpContactTools::Monitor(dm, postProcFe, uXScatter, uYScatter));
    boost::shared_ptr<ForcesAndSourcesCore> null;
    CHKERR DMMoFEMTSSetMonitor(dm, solver, simple->getDomainFEName(),
                               monitor_ptr, null, null);
    MoFEMFunctionReturn(0);
  };

  CHKERR set_section_monitor();
  CHKERR create_post_process_element();
  uXScatter = scatter_create(0);
  uYScatter = scatter_create(1);
  CHKERR set_time_monitor();

  CHKERR TSSolve(solver, D);

  CHKERR VecGhostUpdateBegin(D, INSERT_VALUES, SCATTER_FORWARD);
  CHKERR VecGhostUpdateEnd(D, INSERT_VALUES, SCATTER_FORWARD);
  CHKERR DMoFEMMeshToLocalVector(dm, D, INSERT_VALUES, SCATTER_REVERSE);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode ContactPlasticity::postProcess() { return 0; }

MoFEMErrorCode ContactPlasticity::checkResults() { return 0; }

Range ContactPlasticity::getEntsOnMeshSkin() {
  Range faces;
  CHKERR mField.get_moab().get_entities_by_type(0, MBTRI, faces);
  Skinner skin(&mField.get_moab());
  Range skin_edges;
  CHKERR skin.find_skin(0, faces, false, skin_edges);
  return skin_edges;
};

static char help[] = "...\n\n";

int main(int argc, char *argv[]) {

  MoFEM::Core::Initialize(&argc, &argv, (char *)0, help);

  try {

    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);

    moab::Core mb_instance;              ///< mesh database
    moab::Interface &moab = mb_instance; ///< mesh database interface

    MoFEM::Core core(moab);           ///< finite element database
    MoFEM::Interface &m_field = core; ///< finite element database insterface

    Simple *simple = m_field.getInterface<Simple>();
    CHKERR simple->getOptions();
    CHKERR simple->loadFile("");

    // CHKERR get_roller_parameters(m_field);

    CHKERR get_options_from_command_line();

    ContactPlasticity ex(m_field);
    CHKERR ex.runProblem();
  }
  CATCH_ERRORS;

  CHKERR MoFEM::Core::Finalize();
}
