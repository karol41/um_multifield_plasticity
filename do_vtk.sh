#!/bin/sh


for H5M_FILE in out_*h5m
do

BASE_NAME=`basename $H5M_FILE .h5m`


VTK_FILE=$BASE_NAME.vtk
  if [ $VTK_FILE -nt $H5M_FILE ]; then
    echo $H5M_FILE is older than $VTK_FILE
  else
    echo $H5M_FILE convert to $BASE_NAME.vtk
    mbconvert $H5M_FILE $VTK_FILE
  fi

done