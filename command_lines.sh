
# BEAM
rm -f out_* && make -j6 && ./multifield_plasticity -file_name ~/Desktop/Meshes/Plasticity/2D_cantilever.cub -ts_dt 0.04 -ts_final_time 0.56 -ts_adapt_type none \
-with_plasticity -load_history load_history.in \
-young 210e3 \
-poisson 0.3 \
-sigmaY 240.0 \
-H 1e-2 \
-order 2 -no_output | tee log



rm -f out_* && make -j6 && ./multifield_plasticity -file_name ~/Desktop/Meshes/Plasticity/2D_contact_circ.cub -ts_dt 0.04 -ts_final_time 1 -ts_adapt_type none -ts_adapt_dt_max 0.2 -ts_adapt_dt_min 0.005   | tee log

# Mikey mouse
./multifield_plasticity -file_name ~/Desktop/Meshes/Plasticity/2D_contact_circ.cub -with_contact -ts_final_time 3  -load_history load_history.in -roller1 1,-1.3 -radius1 0.5 -move1 -0.15,0. -roller2 -1,,-1.3 -move2 0.15,0 -radius2 0.5 | tee log && mbconvertp out_*h5m


# punch example
rm -f out_* && make -j6 && ./multifield_plasticity -file_name ~/Desktop/Meshes/Plasticity/2D_bar_contact_test.cub -ts_dt 0.05 -ts_final_time 3 -ts_adapt_type none \
-with_contact -load_history load_history2.in \
-roller3 0.,0.5 -move3 0.,-0.25 -radius3 0.25 \
-gravity_y 0.01 | tee log


-roller1 1.25,-0.5  -radius1 0.25 \
-roller2 -1.25,-0.5 -radius2 0.25 \

grep "Uy time" log | awk '{print $3 " " $5}' | tee log_upd

gnuplot -e "set terminal x11; plot 'log_upd' using (-\$2):(\$1) with linespoints; pause -1"

